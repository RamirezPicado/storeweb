﻿using Infraestructure.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infraestructure.Repository
{
    public class RepositoryDistribuidor : IRepositoryDistribuidor
    {
        public IEnumerable<Distribuidor> GetDistribuidor()
        {
            try
            {
                IEnumerable<Distribuidor> lista = null;
                using (MyContext ctx = new MyContext())
                {
                    ctx.Configuration.LazyLoadingEnabled = false;
                    // SELECT * FROM producto
                    lista = ctx.Distribuidor.ToList<Distribuidor>();
                }
                return lista;
            }
            catch (DbUpdateException dbEx)
            {
                string menseje = "";
                Log.Error(dbEx, System.Reflection.MethodBase.GetCurrentMethod());
                throw new Exception(menseje);
            }
            catch (Exception ex)
            {
                Log.Error(ex, System.Reflection.MethodBase.GetCurrentMethod());
                throw;
            }
        }

        public Distribuidor GetDistribuidorByID(int id)
        {
            throw new NotImplementedException();
        }
    }
}
