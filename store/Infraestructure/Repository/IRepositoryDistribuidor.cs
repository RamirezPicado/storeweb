﻿using Infraestructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infraestructure.Repository
{
    public interface IRepositoryDistribuidor
    {
        IEnumerable<Distribuidor> GetDistribuidor();
        Distribuidor GetDistribuidorByID(int id);
    }
}
