﻿using Infraestructure.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infraestructure.Repository
{
    public class RepositoryProducto: IRepositoryProducto
    {
        public IEnumerable<Producto> GetProducto()
        {
            try
            {
                IEnumerable<Producto> lista = null;
                using (MyContext ctx = new MyContext())
                {
                    ctx.Configuration.LazyLoadingEnabled = false;
                    // SELECT * FROM producto
                    lista = ctx.Producto.ToList<Producto>();
                }
                return lista;
            }
            catch (DbUpdateException dbEx)
            {
                string menseje = "";
                Log.Error(dbEx, System.Reflection.MethodBase.GetCurrentMethod());
                throw new Exception(menseje);
            }
            catch (Exception ex) 
            {
                Log.Error(ex, System.Reflection.MethodBase.GetCurrentMethod());
                throw;
            }
        }

        Producto IRepositoryProducto.GetProductoByID(int id)
        {
            throw new NotImplementedException();
        }
    }
}
